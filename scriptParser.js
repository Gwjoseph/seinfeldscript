var fs = require('fs');

var scriptArray;
var georgeLineCount=0;
var jerryLineCount=0;
var kramerLineCount=0;
var elaineLineCount=0;

var georgeSceneCount=0;
var jerrySceneCount=0;
var kramerSceneCount=0;
var elaineSceneCount=0;



scriptArray = fs.readFileSync('S01E01.txt').toString().split("\n");

scriptArray.forEach(element => {
    if(element.toLowerCase().includes("jerry: ")){
        jerryLineCount++;
    }else if(element.toLowerCase().includes("george: ")){
        georgeLineCount++;
    }else if(element.toLowerCase().includes("elaine: ")){
        elaineLineCount++;
    }else if(element.toLowerCase().includes("kramer: ")){
        kramerLineCount++;
    }
});


var sceneArray = fs.readFileSync('S01E01.txt').toString().split("[Scene: ");
sceneArray.forEach(element => {
   // console.log("new scene is ");
   // console.log(element);
    if(element.toLowerCase().includes("jerry: ")){
        jerrySceneCount++;
    }
    if(element.toLowerCase().includes("george: ")){
        georgeSceneCount++;
    }
    if(element.toLowerCase().includes("elaine: ")){
        elaineSceneCount++;
    }
    if(element.toLowerCase().includes("kramer: ")){
        kramerSceneCount++;
    }
});

var sceneCount = sceneArray.length;
console.log("SEASON 01 EPISODE 01 The Seinfeld Chronicles ");
console.log("Jerry had " + jerryLineCount + " lines and was in " +  jerrySceneCount + " out of the " + sceneCount + " scenes.");
console.log("George had " + georgeLineCount + " lines and was in " +  georgeSceneCount + " out of the " + sceneCount + " scenes.");
console.log("Kramer had " + kramerLineCount + " lines and was in " +  kramerSceneCount + " out of the " + sceneCount + " scenes.");
console.log("Elaine had " + elaineLineCount + " lines and was in " +  elaineSceneCount + " out of the " + sceneCount + " scenes.");

