var fs = require('fs');
const https = require('https');
require('dotenv').config()

const scriptDirectory = process.env.SCRIPT_DIRECTORY || 'C:/Users/geo22/Documents/Workspace/seinfeldScriptData/scripts/';

var uriArray = fs.readFileSync('scriptURLs.txt').toString().split("\r\n");
console.log(uriArray);

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

async function downloadScriptFiles() {
    for (let index = 0; index < uriArray.length; index++) {
        let element = uriArray[index];
        let uriPath = encodeURI('/' + element);
        console.log(element);
        console.log(uriPath);
        let options = {
            hostname: 'www.seinfeldscripts.com',
            path: uriPath,
        };
        options.agent = new https.Agent(options);

        const fileDownload = new Promise((resolve, reject) => {
            const req = https.get(options, (res) => {
                let fileName = element.replace('.html','');
                fileName = fileName.replace('.htm','');
                fileName = fileName + '.txt';

                let data = '';
                res.on('data', function (chunk) {
                    data += chunk;
                });
                res.on('end', function () {
                    fs.writeFileSync(scriptDirectory + fileName, data, err=> {
                        if(err){
                            console.error(err);
                            reject(`Failed to write ${fileName}`);
                        }
                    });
                    resolve(`Downloaded ${fileName}`);
                });
            });

            req.on('error', (e) => {
                console.error(e);
                reject(`Failed to download ${fileName}`);
            });

            req.end();
        });

        await fileDownload;
        // Sleep for 0.5 seconds so we don't DDoS them Kappa
        await sleep(500);
    }
}

downloadScriptFiles();
